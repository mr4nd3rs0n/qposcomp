const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const testSchema = new Schema({
  year: { type: Number, unique: true }
}, {
  timestamps: true
});

var Test = mongoose.model('Test', testSchema);

module.exports = Test;
