const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const questionSchema = new Schema({
  enunciation: { type: String },
  test: { type: mongoose.Schema.Types.ObjectId, ref: 'Test' },
  number: { type: Number },
  image: { type: String },
  area: { type: String, enum: ['Matemática', 'Fundamentos da Computação', 'Tecnologias da Computação'] },
  dificulty: { type: String, enum: ['Muito Fácil', 'Fácil', 'Normal', 'Difícil', 'Muito Difícil'] },
  correctAlternative: { type: Number },
  alternatives: [String]
}, {
  timestamps: true
});

var Question = mongoose.model('Question', questionSchema);

module.exports = Question;
