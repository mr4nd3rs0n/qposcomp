var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
var config = require('./config');
var cors = require('cors');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var questionRouter = require('./routes/questionRouter');
var testRouter = require('./routes/testRouter');

var Test = require('./models/tests');
var Question = require('./models/questions');

const url = config.mongoUrl;
const connect = mongoose.connect(url, {
  useNewUrlParser: true
});

connect.then((db) => {
  console.log('Connected correctly to server');
}, (err) => { console.log(err); });

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());

app.use('/', indexRouter);
app.use('/api/users', usersRouter);
app.use('/api/questions', questionRouter);
app.use('/api/tests', testRouter);

//handle production
if ( process.env.NODE_ENV === 'production'){
  // static folder
  app.use(express.static(__dirname + '/public/'));

  // handle single page application
  app.get(/.*/, (req, res) => res.sendFile(__dirname + '/public/index.html'));
}

/*var provaTeste = new Test({
  year: 2002
}).save().then((prova) => console.log(prova));

var questaoTeste = new Question({
  enunciation: 'Uma característica de uma arquitetura RISC é:',
  test: mongoose.Types.ObjectId('5c82e58779a5de1d3c4ac4ba'),
  number: 21,
  area: 'Fundamentos da Computação',
  dificulty: 'Normal',
  correctAlternative: 3,
  alternatives: ['Uma arquitetura de alto risco pois o mercado de hardware evolui muito importante',
                  'Possui um grande conjunto de instruções complexas',
                  'A arquitetura é constituída de milhares de processadores',
                  'Possui um pequeno conjunto de instruções simples',
                  'O processador é formado por válvulas e transistores' ]
}).save().then((questao) => console.log(questao));*/

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
