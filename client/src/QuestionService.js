import axios from 'axios';

const url = 'api/questions/';
//get questions
class QuestionService {

  // Get questions
  static getQuestions(filtro){
    return new Promise( async(resolve, reject) => {
      try {
        const res = await axios.get(url);
        const data = res.data;
        resolve(
          data.map(post => ({
            ...post,
            createdAt: new Date(post.createdAt)
          }))
        );
      } catch(err){
        reject(err);
      }
    });
  }

  static verifyAnswer(questionId, alternative){
    return axios.get(`${url}/${questionId}/${alternative}`);
  }
}

export default QuestionService;
